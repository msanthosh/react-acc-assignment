import React, { Component } from 'react';

class App extends Component{

   constructor(){
      super()
      this.state = {
         data : ["Hello World", "Hello World"],
         duplicateString : ["abc", "def", "ghi","abc", "def"],
         objectKeys : {
            a : 1,
            b : 2,
            c: [{
               d:4
            },,{
               e:5,
               f: {
                  g : 6
               }

            }]
         },
         dataResult : ""
      }
   }

   returnCharacterCount(){
      let strArr = this.state.data; // "let" is a keyword which allows us to declare a variable with block scope.
      let charObj = {}
      strArr.forEach((str)=>{
         str.split('').forEach((char)=>{
            charObj[char] = (charObj[char] || 0) + 1;
         })
      })

      this.setState({dataResult : JSON.stringify(charObj)})
      // console.log(JSON.stringify(charObj)); // Output : {"H":2,"e":2,"l":6,"o":4," ":2,"W":2,"r":2,"d":2}
   }


   getObjectKeys(obj){
      let keys = [];
     
      Object.keys(obj).forEach((key) => {
         keys.push(key);
         if(typeof obj[key] === "object"){
            let nextLevelKeys = this.getObjectKeys(obj[key]);
            keys = keys.concat(nextLevelKeys.map(function(nextLevelKey) {
               return key + "." + nextLevelKey;
            }));
         }
      })
      // console.log(keys) 
      this.setState({dataResult : keys})
      return keys; // Output : ["a","b","c","c.0","c.0.d","c.2","c.2.e","c.2.f","c.2.f.g"]
   }

   reverseString(){
      let string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
      let revString = string.split("").reverse().join("")
      this.setState({dataResult : revString})
      // console.log(revString) // Output : .tile gnicsipida rutetcesnoc ,tema tis rolod muspi meroL
   }

   removeDuplicate(){
      let strArr = this.state.duplicateString;
      let uniqueArr = [];
      console.log(this.state.duplicateString)
      strArr.forEach((str, index)=> {
         if(uniqueArr.includes(str)){
            strArr.splice(index, index+1);
         }else{
            uniqueArr.push(str)
         }
      })
      
      this.setState({dataResult : "Removed Duplicates : " + strArr})
      console.log(this.state.duplicateString)
   }

   render(){
      return(
         <div>
            <h1>Assignment</h1>
            <p> Output : {this.state.dataResult}</p> {/* "this" operator is the reference object of an current execution context. */}
            <button onClick={this.returnCharacterCount.bind(this)}> Get Character Count</button>
            <button onClick={this.getObjectKeys.bind(this, this.state.objectKeys)}> Get Object Keys </button>
            <button onClick={this.removeDuplicate.bind(this)}> Remove Duplicates </button>
            <button onClick={this.reverseString.bind(this)}> Reverse String </button>
         </div>
      );
   }
}
export default App;